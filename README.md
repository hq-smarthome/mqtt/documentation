# MQTT Setup

This readme will go over the structure and plans of the MQTT setup within the hQ household ranging from how topic names are created to types of devices in use.

[[_TOC_]]

## Client Ids

This setup makes use of client ids when authenticating and for forming the topic names.

### Format

The basic structure of the client id is:
```
{deviceType}/{location}
```
Where `deviceType` describes the device itself e.g. `ups`, `display`, or 'environment`

and where `location` is the logical location for the device e.g `living-room`, `hallway`, or `front-door`

### Examples

Some full examples can include:
* `ups/living-room`
* `ups/server-rack`
* `display/kitchen`

## Topics

Topic names are formed 

### Format

The current convention for the topic name is as follows: 
```
hq/{clientId}/{sensorPath}
```

where `clientId` is as defined in [ClientIds](#clientids)

and where `sensorPath` is the logical path that describes the value e.g. for an environment sensor measuring temperature: `hq/{clientId}/temperature` or for something with many values a namespace can be used e.g. a ups with battery statistics could have `hq/{clientId}/battery/voltage`

### Examples

* `hq/ups/living-room/battery/voltage`
* `hq/ups/living-room/battery/charge`
* `hq/display/front-door/display/brightness`
* `hq/display/front-door/display/on`

## Devices

This will contain most of the currently active devices that connect to MQTT along with links to their build repositories (if they exist)

### UPS

#### Server Rack

Build Repository: *none*

Client Id: `ups/server-rack`

Subscriptions: *none*

Publishing Topics: *none*


### Dashboards

#### Front Door

Build Repository: *none*

Client Id: `dashboard/front-door`

Subscriptions:
* `hq/dashboard/front-door/display/on` Turns the display on for 2 minutes
* `hq/dashboard/front-door/display/brightness` Turns the display to a supplied brightness

Publishing Topics: *none*


### Drawers

#### Living Room

Build Repositiory: [Drawer Sense](https://gitlab.com/hq-smarthome/mqtt/drawer-sense)

Client Id: `cabinet/living-room`

Subscriptions:
* `hq/cabinet/update` Return current status of all drawers for all cabinets
* `hq/cabinet/living-room/update` Return current status of all drawers for this cabinet

Publishing Topics:

*All the following emit either `open` or `closed`*

* `hq/cabinet/living-room/drawers/0`
* `hq/cabinet/living-room/drawers/1`
* `hq/cabinet/living-room/drawers/2`
* `hq/cabinet/living-room/drawers/3`
* `hq/cabinet/living-room/drawers/4`
* `hq/cabinet/living-room/drawers/5`


## Authentication

Currently authentication is done using clientId/username and password, this will be the case until I get around to enabling cert based authentication on all the devices. All passwords are generated with a large length (so no `test123456` sorry...).

## Connections

The broker is configured to require all connections to be done over TLS thus all devices have to follow this requirement.

This does mean that I will be unable to use very low power devices (e.g. Arduino UNO & WiFi Shield) as they don't have the required processing power to handle the TLS handshake but this is not an issue for me in the long run.

In addition to using TLS, all devices are required (by me at least) to be able to verify the CA and certificate in use by the broker thus putting the requirement that hostname checking on the broker is done too (otherwise the cert will be invalid).
